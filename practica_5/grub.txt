1. Múltiples menús

● Practicar l’arrencada amb els següents timeouts: 5, 0 i -1.
El que passa és que el temps que el sistema espera a l'interacció de l'user canvia, 5 (s'espera 5 segons), (0 no s'espera i entra directament al menu per defecte), (-1 s'espera indefinidament)

● Practicar l’arrencada modificant el amb set default amb els valors 0 i 2.
El que passa es que amb 0 el menu per defecte és el primer i amb 2 és el tercer.

● Crear el menú:
  ○ F27 per defecte

menuentry 'Fedora (4.13.9-300.fc27.x86_64) 27 (Cloud Edition)' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.13.9-300.fc27.x86_64-advanced-d020d197-657b-4030-bde5-0db8417bc39c'{
	load_video
	set gfxpayload=keep
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos1'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint='hd0,msdos1' d020d197-657b-4030-bde5-0db8417bc39c
	else
	  search --no-floppy --fs-uuid --set=root d020d197-657b-4030-bde5-0db8417bc39c
	fi
	linux16 /boot/vmlinuz-4.13.9-300.fc27.x86_64 root=UUID=d020d197-657b-4030-bde5-0db8417bc39c ro no_timer_check console=tty1 console=ttys0,115200n8 LANG=en_US.UTF-8
	initrd /boot/initramfs-4.13.9-300.fc27.x86_64.img
}

  ○ F27 despullat

menuentry 'F27 despullat'--unrestricted '{
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos1'
	linux16 /boot/vmlinuz-4.13.9-300.fc27.x86_64 root=UUID=d020d197-657b-4030-bde5-0db8417bc39c ro no_timer_check console=tty1 console=ttys0,115200n8 LANG=en_US.UTF-8
	initrd /boot/initramfs-4.13.9-300.fc27.x86_64.img
}

  ○ F27 rescat

menuentry 'F27 despullat'--unrestricted '{
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos1'
	linux16 /boot/vmlinuz-4.13.9-300.fc27.x86_64 root=UUID=d020d197-657b-4030-bde5-0db8417bc39c ro no_timer_check console=tty1 console=ttys0,115200n8 LANG=en_US.UTF-8 systemd.unit=rescue.target
	initrd /boot/initramfs-4.13.9-300.fc27.x86_64.img
}

  ○ F27 emergency

menuentry 'F27 despullat'--unrestricted '{
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos1'
	linux16 /boot/vmlinuz-4.13.9-300.fc27.x86_64 root=UUID=d020d197-657b-4030-bde5-0db8417bc39c ro no_timer_check console=tty1 console=ttys0,115200n8 LANG=en_US.UTF-8 systemd.unit=emergency.target
	initrd /boot/initramfs-4.13.9-300.fc27.x86_64.img
}

2. Submenús

● Establir el timeout de manera que esperi una selecció i definir la primera opció de
menú com a opció per defecte.

Posar el timeout a -1 i e set default a 0.

● Generar el menú:
	○ F27 per defecte
	○ F27 alternatives
		■ F27 despullat
		■ F27 rescat
		■ F27 emergency
	○ F27 rescue
Per fer un submenu s'ha de fer:
submenu <nom> {
menus[...]
}

3. Mode editar entrada: bàsic
● Editar l’entrada per defecte afegint un 1 al final per entrar en mode rescat.

S'ha de posar l'1 al final de la linia del kernel

● Editar l’entrada per defecte afegint systemd.unit=rescue.target al final.

S'ha de posar "systemd.unit=rescue.target" al final de la linia del kernel

● Editar l’entrada per defecte i esborrar totes les línies. Tornar-les a escriure només les
bàsiques necessàries per a una entrada ‘despullada’.

menuentry <titol> 
	insmod gzio
	insmod part_msdos
	isnmod ext2
	set root='hd0,msdo1'
	linux16...
	initrd16...

4. Generar menús automàticameny: grub2-mkconfig

● Usant l’ordre ​ grub2-mkconfig mostrar per stdout el menú de grub2 que es genera
automàticament amb aquesta eina.

grub2-mkconfig

● Fer el mateix generant el fitxer grub.conf.out usant un redireccionament de sortida.

grub2-mkconfig > grub.conf.out

● Desar una còpia de seguretat del fitxer del menú de grub actual ​ grub.conf
anomenant-la ​ grub.unso.conf . ​

cp grub.cfg grub.unso.conf

● Generar un nou fitxer de menú automatitzadament, un nou ​ grub.conf

grub2-mkconfig > grub.cfg

● Verificar entrada

5. Mode Comanda: configfile / llistar

● Des del mode comanda usar l’ordre ​ configfile per seleccionar quin dels fitxers de
menú de grub volem engegar (cal indicar la ruta absoluta device, partition, path i
filename.

configfile(hd0,msdos1)/grub2/.

● Engegar el sistema usant el menú grub.conf.

configfile(hd0,msdos1)/grub2/grub.cfg

● Reboot

● En mode comanda usant l’ordre configfile engegar el sistema utilitzant el menú
grub.unso.conf.

configfile(hd0,msdos1)/grub2/grub.unso.conf

6. Instal·lar un nou sistema: ara qui mana?

● Observen que al instal·lar un nou sistema operatiu el grub d'aquest sobreescriu al del sistema antic al MBR, per aquest motiu veiem que el grub que surt és el del nou sistema operatiu, so volem que tornir a sortir el grub de l'antic sistema hem de fer que mani el grub d'aquest.

7. Establir qui mana (1): local

● Per solucionar el problema de que no apareix el fedora 30 al grub del fedora 27 hem de tornar a fer un grub2-mkconfig -o grub.cfg perquè ara quan faci el xequeix trobarà el fedora 30

8. Iniciar un sistema operatiu no llistat (1): editar una entrada

menuentry 'F30'--unrestricted '{
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos3'
	linux16 /boot/vmlinuz-4.13.9-300.fc27.x86_64 root=/dev/sda3 ro LANG=en_US.UTF-8
	initrd /boot/initramfs-4.13.9-300.fc27.x86_64.img
}

● Per obtenir el kernel i el initram també poder muntar el sistema operatiu a /mnt(per exemple) i veure'ls ahi.

9. Iniciar un sistema operatiu no llistat (2): usar configfile

Teoricament s'ha de fer al command prompt del grub

configfile (hd0,msdos3)/boot/grub2/grub.cfg

i surt el menu de la partició del fedora 30, pero hi ha conflictes i no passa això, en canvi amb dis fedores 27 per exemple si que funciona.

10. Establir qui mana (2): un altre device

● Hem de fer mount de la partició sda3, per exemple a /mnt i fer:

grub2-install --boot-directory=/dev/sda3 /dev/sda

11. Password (1): establir un password a una partició

● Per posar password per poder entrar o editar el menuentry del fedora 27 hem d'espeficar un superuser primer:
set superusers='root'
password root jupiter

i a la part del menuentry teure el "--unrestricted" si està posat.

Si volem que un altre usuari tingui permís també l'hem l'espeficar.

password diego 1234

Per posar les passwords podem fer servir diferens mecanismes que genenern password encriptade per exemple:

grub2-mkpasswd-pbkdf2

12. Password (1): establir un password a una partició

● Per posar password a totes les entrades del grub el que hem der és, després d'assignar el super user i la seva contrasenya, treure el "--unrestricted" de tots el menus.

Grub (2) Recuperació:

13. Eliminació accidental de grub.conf (1): usar backup

● Per iniciar de forma manual (despullada) hem de fer les seguents ordres:
insmod gzio
insmos part_msdos
insmod ext2 

linux16 (hd0,msdos3)/boot/vmlinuz... root=/dev/sda3 ro
initrd16 (hd0,msdos3)/boo/initram...

boot

● Per  poder fer servir algun bk que sabem que tenim hem de fer:

configfile (hd0,msdos3)/boo/grub2/grub.conf.bk

14. Eliminació accidental de grub.conf: usar un altre
sistema

● Amb insmods:
insmod (hd0,msdos1)/boot/grub2/i386-pc/gzio.mod
insmod (hd0,msdos1)/boot/grub2/i386-pc/part_msdos.mod
insmod (hd0,msdos1)/boot/grub2/i386-pc/ext2.mod

linux16 (hd0,msdos1)/boot/vmlinuz... root=/dev/sda1 ro
initrd16 (hd0,msdos1)/boot/initramfs...

boot

● Amb normal:
set root=(hd0,msdos1)
set prefix=(hd0,msdos1)/boot/grub2
insmod normal
normal

i després d'haver fet 1 de les dues soluciones mostrades:

grub2-install /dev/sda

16. Eliminació accidental de la partició de grub

mount /dev/sda1 /mnt

grub2-install --boot-directory=/mnt/boot /dev/sda
