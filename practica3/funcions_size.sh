#! /bin/bash 
#edt
#Diego sanchez
#isx2031424
#asix m01
#-----------------------------------------------------
#(8.1)Ampliar filtrant només els grups
#del 0 al 100.
function allgroupsize2(){
  groupList=$(cat /etc/group | sort -t: -k3g,3)
  for line in $groupList; do
    gid=$(echo $line | cut -d: -f3)
    if [ $gid -le 100 ]; then
      echo $line
      homeList=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f6)
      for home in $homeList; do
        du -hs $home 2> /dev/null | sed -r 's/(.*)/\t\1/'
      done
    fi
  done
}
#-------------------------------------------------------
#(8)​ allgroupsize​ Llistar totes les línies de /etc/group i per cada llínia llistar
#l'ocupació del home dels usuaris que hi pertanyen.
function allgroupsize(){
	groupList=$(cat /etc/group | sort -t: -k3g,3)
  for line in $groupList; do
    gid=$(echo $line | cut -d: -f3)
    echo $line
    homeList=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f6)
    for home in $homeList; do
      du -hs $home 2> /dev/null | sed -r 's/(.*)/\t\1/'
    done
  done
}
#---------------------------------------------------------
#(7)​ allgidsize​ Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del
#home dels usuaris que hi pertanyen.
function allgidzise(){
  gidList=$(cut -d: -f4 /etc/passwd | sort -t: -u -k1g,1)
  for gid in $gidList; do
    echo "-----$gid-----"
    gidzise $gid 2> /dev/null
  done
}
#---------------------------------------------------------
#(6)​ gidsize​ Donat un GID com a argument, mostrar per a cada usuari que pertany
#a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i
#que és un gID vàlid.
function gidzise(){
  ERR_NARGS=1
  ERR_GID=2
  OK=0
  #validar nº d'args
  if [ $# -ne 1 ]; then
    echo "error: nº d'args no vàlid" >> /dev/stderr
    echo "usage: $0 gid" >> /dev/stderr
    retrun $ERR_NARGS
  fi
  #validar gid
  gid=$1
  egrep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "error: gid $gid no vàlid" >> /dev/stderr
    return $ERR_GID
  fi
  #xixa
  listaHome=$(egrep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | cut -d: -f6)
  for home in $listaHome; do
    du -hs $home
  done
  return $OK
}
#---------------------------------------------------------------
#(5)​ grepgid​ Donat un GID com a argument, llistar els logins dels usuaris que
#petanyen a aquest grup com a grup principal. Verificar que es rep un argument i
#que és un GID vàlid.
function grepgid(){
  ERR_NARGS=1
  ERR_GID=2
  OK=0
  #validar nº d'args
  if [ $# -ne 1 ]; then
    echo "error: nº d'args incorrecte" >> /dev/stderr
    echo "usage: $0 gid" >> /dev/stderr
    return $ERR_NARGS
  fi
  #validar gid
  gid=$1
  egrep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "error: gid $gid no vàlid" >> /dev/stderr
    return $ERR_GID
  fi
  egrep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd
  return $OK
}
#-----------------------------------------------------------
#(4)​ loginboth​ Rep com a argument un file o res (en aquest cas es processa stdin).
#El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de
#l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.
function loginboth(){
  ERR_NARGS=1
  status=0
  
  fileIn=$1
  if [ $# -ne 1 ]; then
    fileIn=/dev/stdin
  fi
  while read -r login; do
    egrep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "error: login $login no existeix" >> /dev/stderr
      status=$((status+1))
    else
      fsize $login
    fi
  done < $fileIn
  return $status
}
#------------------------------------------------------------
#(3)​ loginfile​ Rep com a argument un nom de fitxer que conté un lògin per línia.
#Mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un
#argument i que és un regular file.
function loginfile(){
  ERR_NARGS=1
  ERR_RFILE=2
  OK=0
  #validar nº args
  if [ $# -ne 1 ]; then
    echo "error: nº d'args incorrecte" >> /dev/stderr
    echo "usage: $0 logins_file" >> /dev/stderr
    return $ERR_NARGS
  fi
  #validar que es un regular file
  file=$1
  if ! [ -f $file ]; then
    echo "error: el file $file no es un regular file o no existeix" >> /dev/stderr
    return $ERR_RFILE
  fi
  #xixa 
  while read -r login; do
    fsize $login
  done < $file
  return $OK
}
#-------------------------------------------------------
#(2)​ loginargs​ Aquesta funció rep logins i per a cada login es mostra l'ocupació de
#disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument.
#Per a cada argument verificar si és un login vàlid, si no generra una traça d'error.
function loginargs(){
  ERR_NARGS=1
  status=0
  #validar args
  if [ $# -lt 1 ]; then
    echo "error: no logins given" >> /dev/stderr
    echo "usage: $0 login[...]"
    return $ERR_NARGS
  fi
  #xixa
  loginList=$*
  for login in $loginList; do
    egrep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "error: login $login no existeix" >> /dev/stderr
      status=$((status+1))
    else
      fsize $login
    fi
   done
   return $status
}

#---------------------------------------------------------
#(1)​ fsize​ Donat un login calcular amb du l'ocupació del home de l'usuari.
#Cal obtenir el home del /etc/passwd.
function fsize(){
  login=$1
  home=$(egrep "^$login:" /etc/passwd | cut -d: -f6)
  du -hs $home
}
