#!/usr/bin/python3
#-*- coding: utf-8-*-
#diego sanchez piedra
#isx2301424
#versió 1
#-----------------------------------
#(11)​ allfstypeif​ LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint. Es rep un
#valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype
#que hi ha d'haver per sortir al llistat.
fsList=$(egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
| cut -d: -f3 | sort -u)
for fs in $fsList; do
  wc=$(egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
  | egrep "^[^:]*:[^:]*:$fs:" | wc -l)
  if [ $wc -ge $1 ]; then
	echo "-------------fstype line-----------------------"
	egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
	| egrep "^[^:]*:[^:]*:$fs:" | tr -s ':' '\t'
	echo "-------------dev and mount point-----------------------"
    egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
    | egrep "^[^:]*:[^:]*:ext4:" | cut -d: -f1,2 | tr -s ':' '\t'
  fi
done
exit 0
#-------------------------------------
#(10)​ allfstype​ LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
fsList=$(egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
| cut -d: -f3 | sort -u)
for fs in $fsList; do
  echo "-------------fstype line-----------------------"
  egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
  | egrep "^[^:]*:[^:]*:$fs:" | tr -s ':' '\t'
  echo "-------------dev and mount point-----------------------"
  egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
  | egrep "^[^:]*:[^:]*:ext4:" | cut -d: -f1,2 | tr -s ':' '\t'
done
exit 0
#--------------------------
#(9)​ fstype​ Donat un fstype llista el device i el mountpoint (per odre de device) de
#les entrades de fstab d'quest fstype.

fstype=$1
egrep -v "^#|^# " /etc/fstab | tr -s '[:blank:]' ':' \
| egrep "^[^:]*:[^:]*:$fstype:" | sort -t: -k1 | tr -s ':' '\t'
exit 0
