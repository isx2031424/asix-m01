1. Particions bàsiques: ntfs i ext4

● Per poder canviar el format del fs a ntfs, a fedora minimal no ve instal·lat, hem d'instal·lar ntfsprogs i fer: mkfs.ntfs -f /dev/sda, muntar a /mnt i per ultim fer el dd per ocupar 500MB: dd if=/dev/zero of=coses bs=1K count=500K i l'access.log: date > access.log i desmuntar

● Posar sda3 a format ext4: mkfs.ext4 /dev/sda3, muntar a /mnt i fer dd if=/dev/zero of=coses bs=1K count=500K i per ultim date > access.log i desmuntar

● Les dues particions tenen la mateixa quantita de sectors ocupats: 4194304

2. Imatge d’una partició / Moure una partició

● dd if=/dev/sda2 of=windows.img

● dd if=/dev/sda3 of=/dev/sda2

● losetup /dev/loop0 /dev/sda2
  mount /dev/loop0 /mnt

3. Planxat d’una imatge dd

● dd if=windows.img of=/dev/sda3

● losetup /dev/loop0 /dev/sda3
  mount /dev/loop0 /mnt

4. Shrink d’un fs i una partició

● Abans de fer el resize s'ha de fer fsck:
  e2fsck -f /dev/sda2
  resize2fs /dev/sda2 1G

● Sector d'inici de la partició: 10487808
  Esborrem la partició i tornem a crear amb el mateix sector d'inici, però ara amb 1GB

● losetup /dev/loop0 /dev/sda2
  mount /dev/loop0 /mnt
  cd /mnt
  date > access.log

5. Grow d’un fs i una partició

● Sector d'inici de la partició: 10487808
  Esborrem la partició i tornem a crear amb el mateix sector d'inici, però ara amb 3GB

● Abans de fer el resize s'ha de fer fsck:
  e2fsck -f /dev/sda2
  resize2fs /dev/sda2 3G

● losetup /dev/loop0 /dev/sda2
  mount /dev/loop0 /mnt
  date >> access.log

6. Planxar de nou una imatge

● dd if=windows.img of=/dev/sda3

Particions(2): Dos sistemes operatius

● qemu-kvm -m 2048 -smp 2 -hda F27hisx1_2.qcow2 -cdrom Fedora-Workstation-netinst-x86_64-30-1.2.iso -boot once=d 2> /dev/null &

● qemu-img create -f qcow2 -b F27hisx1_2.qcow2 F27hisx1_2_bk.qcow2 

8. Grow de F30

● Esborrem la partició sda2 i la tornem a crear amb 3GB tenint en compte que el sector d'inici ha de ser el mateix que el d'abans

● Fem el e2fsck: e2fsck /dev/sda2 i el resize: resize2fs /dev/sda2 3G

9. Shrink de F30

● Fem el fsck: e2fsck /dev/sda2 i reduim el fs amb resize: resize2fs /dev/sda2 2G

● Esborrem la partició i la tornem a crear amb 2G i tenint en compte que el sector d'inici sigui igual que el d'abans

10. Moure un sistema operatiu.

● He fet les particions dues particios de 2GB ja que em diu que no hi ha mes espai

● dd if=/dev/sda2 of=/dev/sda3

● dd if=/dev/zerp of=/dev/sda2

11. Engegar F30

● set root=(hd0,msdos1)
  set prefix=(hd0,msdos1)/boot/grub2
  insmod normal
  normal

● Com el fedora 27 de la particio sda1 no té el fedora30 trobat hem de fer: grub2-mkconfig -o grub.cfg i trobarà el fedora 30, per ultim hem de fer que mani aquesta partició: grub2-install /dev/sda

● reboot

12. Crear una partició de swap

● A fdisk hem de donar-li a la t perque canviar a format swap que és el codi 82 (Linux swap / Solaris)

● Al fstab del fedora 30 posem:
  /dev/sda2 swap                         swap    defaults        0 0

13. Moure una partició a l’esquerra

● Arrenquem la màquina virtual amb el live del gparted, qemu-kvm -m 2048 -smp 2 -hda F27hisx1_2.qcow2 -cdrom gparted-live-1.1.0-1-i686.iso -boot once=d 2> /dev/null &

● Seleccionem la particio que volem moure, i li donem a l'opció "resize/move" i la movem a l'esquerra deixant 0 bytes d'espai a l'esquerra, guardem i sortim del live.


14. Et voilà

● Iniciem el fedora27 que es troba al sda1, aquesta partició no ha sigut modificada, tornem a generar el grub: grub2-mkconfig -o /boot/grub2/grub.cfg i fem que mani aquesta partició: grub2-install /dev/sda.

● Posem al fstab de sda1:
  /dev/sda2 swap                         swap    defaults        0 0

