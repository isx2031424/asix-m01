#! /bin/bash 
#Diego sanchez
#asix m01
#del_usuari
#programa que rep un login i eleiminta tot el que pertany a l'usuari per deixant un tar.gz de tots el fitxers que li pertanyen.

ERR_NARGS=1
ERR_NOLOGIN=2
OK=0
#validar que rep un arg
if [ $# -ne 1 ]; then
  echo "error nº args $# incorrecte" >> /dev/stderr
  echo "usage: $0 login" >> /dev/stderr
  exit $ERR_NARGS
fi

#validar que el login existeix
login=$1
egrep "^$login:" /etc/passwd &> /dev/null
if [ $? -ne 0 ]; then
  echo "el login $login no existeix" >> /dev/stderr
  exit $ERR_NOLOGIN
fi

#acabar el processos que el login donat té
pkill -U $(id -u $login) && echo "processos terminats"

#guardar els seus fitxers en un tar
tar -cvzf $login-files.tgz $(find / -user $login 2> /dev/null) &> /dev/null && echo "fitxers desats" >> /dev/stderr

#esborrar l'usuari
userdel -r $login && echo "usuari esborrat" >> /dev/stderr

exit $OK
